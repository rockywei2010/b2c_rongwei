/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 27/10/14
 */

/**
 * Score layer, it displays 2 or 1 score on the upper corners of screen
 * it also has a balance bar, showing score weight aginst each other
 * at the end of the game, the score sequence should be post to server
 * @type {cc.Layer.extend}
 */
//TODO: onExit -> post time-score data to server
var ScoreBoardLayer = cc.Layer.extend({
    ctor: function() {
        /*TODO： UI, balance bar etc*/
        this._super();
        var self = this;
        var score = {player1: 0, player2: 0};
        /* UI layout */
        /* TODO: refactor */
        var scoreTextMap = {
            player1: new cc.LabelTTF(score.player1.toString(), "Arial", 42),
            player2: new cc.LabelTTF(score.player2.toString(), "Arial", 42),
            scorelabel1: new cc.LabelTTF(l18n.getString('score'), "Arial", 42),
            scorelabel2: new cc.LabelTTF(l18n.getString('score'), "Arial", 42)
        };
        scoreTextMap.player1.x = scoreTextMap.scorelabel1.getBoundingBox().width + 10 + 120;
        scoreTextMap.player1.y = cc.winSize.height - 120;
        scoreTextMap.scorelabel1.x = 120;
        scoreTextMap.scorelabel1.y = cc.winSize.height - 120;

        scoreTextMap.player2.x = cc.winSize.width - 120;
        scoreTextMap.player2.y = cc.winSize.height - 120;
        scoreTextMap.scorelabel2.x = cc.winSize.width - 120 - 10 - scoreTextMap.scorelabel2.getBoundingBox().width;
        scoreTextMap.scorelabel2.y = cc.winSize.height - 120;

        _.values(scoreTextMap).forEach(function(l) {
            l.color = new cc.Color(35, 52, 66, 255);
            self.addChild(l)
        });
        /* end UI layout */
        self.timer = new GameTimer(gameConfig.gameDuration, 30, 60, function() {
            cc.eventManager.dispatchCustomEvent(GameEvents.timeUp, {});
        });
        self.timer.x = cc.winSize.width / 2;
        self.timer.y = cc.winSize.height - 120;
        self.timer.titleText.setString(l18n.getString('timer'));
        self.timer.reset();

        /*
         * The event userdata should contains:
         * 1. Score it changed
         * 2. Player
         *  object {player:str, name:str ,score: int}]
         * @type {{event: number, eventName: (GameEvents.scoreUpdate|*), callback: Function}}
         */
        var scoreUpdateEventUpdateListener = {
            event: cc.EventListener.CUSTOM,
            eventName: GameEvents.scoreUpdate,
            callback: function(event) {
                var evt = event.getUserData();
                var curScore = parseInt(score[evt.player]) + evt.score;
                score[evt.player] = curScore > 0 ? curScore : 0;
                scoreTextMap[evt.player].setString(score[evt.player].toString());
            }
        };
        var gameFinishListener = {
            event: cc.EventListener.CUSTOM,
            eventName: GameEvents.gameFinished,
            callback: function(event) {
                var evt = event.getUserData();
                if (!GlobalContext.gameSummary) {
                    GlobalContext.gameSummary = {
                        player1: {name: GlobalContext.currentPlayerName, scores: []},
                        player2: {name: GlobalContext.opponentPlayerName, scores: []}
                    }
                }
                var scoreItem = _.find(GlobalContext.gameSummary.player1.scores, function(s) {
                    return s.game === evt.game;
                });
                if (!scoreItem) {
                    GlobalContext.gameSummary.player1.scores.push({game: evt.game, score: score["player1"]})
                }
                var scoreItem = _.find(GlobalContext.gameSummary.player1.scores, function(s) {
                    return s.game === evt.game;
                });
                scoreItem.score = score["player1"];
            }
        };
        cc.eventManager.addListener(scoreUpdateEventUpdateListener, self)
        cc.eventManager.addListener(gameFinishListener, self)
    },
    onEnter: function() {
        var self = this;
        this._super();
        this.scheduleOnce(function() {
            self.addChild(self.timer);
            self.timer.reset();
            self.timer.start();
        }, 0.5);
    }
});


