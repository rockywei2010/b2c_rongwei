/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 26/11/14
 */

var CountDownLayer = cc.Layer.extend({
    /**
     * This layer displays a countdown in the center of the screen
     * with a localized label on top of the score
     *
     * @param {Int} countDownSeconds
     * @param {Func} callback
     */
    ctor: function(countDownSeconds, callback) {
        this._super();
        var self = this;
        var timer = new GameTimer(countDownSeconds, 72, 150, callback);
        centerSprite(timer);
        self.addChild(timer);
        timer.start();
    }
});

