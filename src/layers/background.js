/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 14/10/14
 */

var BackGroundLayer = cc.Layer.extend({
    sprite: null,
    ctor: function () {
        this._super();
        this.bglayer = new cc.LayerColor(new cc.Color(240, 240, 240, 255),cc.winSize.width, cc.winSize.height);
        this.bglayer.setPosition(new cc.Point(0,0));
        this.addChild(this.bglayer);
    }
});
