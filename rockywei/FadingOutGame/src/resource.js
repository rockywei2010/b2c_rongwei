var res = {
		HelloWorld_png: "res/HelloWorld.png",
		CloseNormal_png: "res/CloseNormal.png",
		CloseSelected_png: "res/CloseSelected.png",
		LoginButton: "res/login_button.png",
		CardBack_png: "res/cardback_almond.png",
		CardBackRed_png: "res/cardback_red.png",
		CardBackBlue_png: "res/cardback_blue.png",
		CardBackBlack_png: "res/cardback_black.png",
		CardBackWhite_png: "res/cardback_white.png",
		CardBackYellow_png: "res/cardback_yellow.png",
		CardBackGreen_png: "res/cardback_green.png",
		CardBackBrown_png: "res/cardback_brown.png",
		ArrowBlue_png: "res/arrow_blue.png",
		ArrowRed_png: "res/arrow_red.png"

};

var g_resources = [];
for (var i in res) {
	g_resources.push(res[i]);
}