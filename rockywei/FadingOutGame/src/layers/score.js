var ScoreBoardLayer=cc.Layer.extend({
	ctor:function(){
		this._super();
		
		var self=this;
		var score={player1:0, player2:0}
		var scoreTextMap = {
			player1: new cc.LabelTTF(score.player1.toString(),"Arial",42),
			player2: new cc.LabelTTF(score.player2.toString(),"Arial",42),
			scorelabel1: new cc.LabelTTF(l18n.getString('score'),"Arial",42),
			scorelabel2: new cc.LabelTTF(l18n.getString('score'),"Arial",42)
		};
		
		scoreTextMap.player1.x=scoreTextMap.scorelabel1.getBoundingBox().width+10+120;
		scoreTextMap.player1.y=cc.winSize.height-120;
		scoreTextMap.scorelabel1.x=120;
		scoreTextMap.scorelabel1.y=cc.winSize.height-120;
		
		scoreTextMap.player2.x=cc.winSize.width-120;
		scoreTextMap.player2.y=cc.winSize.height-120;
		scoreTextMap.scorelabel2.x = cc.winSize.width - 120 - 10 - scoreTextMap.scorelabel2.getBoundingBox().width;
		scoreTextMap.scorelabel2.y = cc.winSize.height - 120;
		
		for (var i = 0; i < Object.keys(scoreTextMap).length; i++) {		
			var element=scoreTextMap[Object.keys(scoreTextMap)[i]];
			element.color = new cc.Color(35, 52, 66, 255);	
			self.addChild(element)
		}		
		
		var scoreUpdateEventUpdateListener={
				event:cc.EventListener.CUSTOM,
				eventName: GameEvents.scoreUpdate,
				callback:function(event){
					var evt=event.getUserData();
					var curScore = parseInt(score[evt.player])+evt.score;
					score[evt.player] = curScore >0 ? curScore:0;
					scoreTextMap[evt.player].setString(score[evt.player].toString());
				}
		};
		cc.eventManager.addListener(scoreUpdateEventUpdateListener, self);
		
		self.timer = new GameTimer(20,30,60,function(){
			cc.eventManager.dispatchCustomEvent(GameEvents.gameFinished, {});
		});
		self.timer.x=cc.winSize.width/2;
		self.timer.y=cc.winSize.height-120;
		self.timer.titleText.setString(l18n.getString('timer'));
		self.timer.reset();
	},
	onEnter:function(){
		var self=this;
		this._super();
		cc.log("where am is");
		this.scheduleOnce(function(){
			self.addChild(self.timer);
			self.timer.reset();
			self.timer.start();
		}, 0.5);
	}
});