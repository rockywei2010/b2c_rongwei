var GameColorOfDeceptionLayer = GameLayer.extend({
	ctor: function() {
		this._super();
		var self = this;
		var levels = {
				8: 2,
				15: 3,
				24: 4
		}
		var countCycle = 0;
		var cardDimension = {
				w: 120,
				h: 120
		};
		var cardspacing = 25;
		var mainBlock = {
				w: cardDimension.w * 2 + cardspacing,
				h: cardDimension.h * 1
		}
		var size = cc.winSize;
		var firstCoordinate = {
				x: (size.width - mainBlock.w + cardDimension.w) / 2,
				y: (size.height - mainBlock.h + cardDimension.h) / 2
		}

		this.reset = function() {
			countCycle++;
			var cardsNum;
			for (var i = 0; i < Object.keys(levels).length; i++) {
				if (countCycle <= Object.keys(levels)[i]) {
					cardsNum = levels[Object.keys(levels)[i]];
					break;
				}
			}

			var randomNum = [];
			this.cards = [];
			//generate one more to get wrong color			
			while (randomNum.length <= cardsNum) {
				var number = Math.floor(Math.random() * Object.keys(CardColorMach).length) + 0;
				var found = false;
				
				for (var i = 0; i < randomNum.length; i++) {
					if (randomNum[i] == number) {
						found = true;
						break
					}
				}
				
				if (!found) {
					randomNum[randomNum.length] = number;
					if (randomNum.length != (cardsNum + 1)) {
						var label = new cc.LabelTTF(Object.keys(CardColorMach)[number], "Arial", 42);
						var card = new Card(cc.winSize.width / 2, cc.winSize.height / 2, label, Games.colorOfDeception);
						card.isReady = true;
						card.setTexture(CardColorMach[Object.keys(CardColorMach)[number]]);
						card.colorMatch = true;
						this.cards.push(card);
					} else {
						this.cards[cardsNum - 1].setTexture(CardColorMach[Object.keys(CardColorMach)[number]]);
						this.cards[cardsNum - 1].colorMatch = false;
						if (Object.keys(CardColorMach)[number] == Colors.white || Object.keys(CardColorMach)[number] == Colors.yellow) {
							this.cards[cardsNum - 1].text.color = new cc.Color(0, 0, 0, 255);
						} else if (Object.keys(CardColorMach)[number] == Colors.black) {
							this.cards[cardsNum - 1].text.color = new cc.Color(255, 255, 255, 255);
						}
					}
				}
			}


			this.cards = shuffle(this.cards);
			for (var i = 0; i < this.cards.length; i++) {
				if (i == this.cards.length - 1 && this.cards.length % 2 == 1) {
					this.cards[i].x = firstCoordinate.x + (cardDimension.w + cardspacing) * ((i + 1) % 2) / 2;
				} else {
					this.cards[i].x = firstCoordinate.x + (cardDimension.w + cardspacing) * (i % 2);
				}

				this.cards[i].y = firstCoordinate.y + (cardDimension.h + cardspacing) * (-1 * Math.floor(i / 2));
				this.addChild(this.cards[i]);
			}
		}
		this.reset();
		cc.eventManager.addListener(gameColorOfDeceptionListener(self, []), self);
	}
});


var gameColorOfDeceptionListener = function(scene, slot) {
	return {
		event: cc.EventListener.CUSTOM,
		eventName: GameEvents.cardTappedColorOfDeception,
		callback: function(event) {
			var card = event.getUserData();
			if (card.colorMatch == false) {
				cc.eventManager.dispatchCustomEvent(GameEvents.scoreUpdate, {
					player: 'player1',
					score: 10
				});
				for (var i = 0; i < scene.cards.length; i++) {
					scene.removeChild(scene.cards[i]);
				}
				setTimeout(function() {
					scene.reset();
				}, 300);
			} else {
				card.runAction(cc.sequence(new cc.Blink(0.5, 3)));
			}
		}
	}
}