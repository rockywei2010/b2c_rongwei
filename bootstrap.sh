#!/bin/bash
# Usage: copy library from cocos2d to project locations

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
type cocos &>/dev/null || echo 'cocos cli is not availble, aborting...'
type cocos &>/dev/null || exit 1
type bower &>/dev/null || echo 'cocos bower is not availble, aborting...'
type bower &>/dev/null || exit 1
pushd $DIR
[ -d .tmp ] || mkdir .tmp
pushd .tmp
cocos new b2c -l js
pushd b2c
mv runtime frameworks tools ../../
pushd $DIR
rm -rf ".tmp"
bower install
