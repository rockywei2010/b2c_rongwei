var BasicScene = cc.Scene.extend({
    bgLayer: null,
    gameListeners: null,
    ctor: function() {
        this._super();
        this.gameListeners = [];
    },
    onEnter: function() {
        var self = this;
        self._super();
        self.bgLayer = new BackGroundLayer();
        if (!_.isEmpty(this.gameListeners)) {
            this.gameListeners.forEach(function(l) {
                cc.eventManager.addListener(l, self);
            });
        }

        this.addChild(self.bgLayer);
    },

    onExit: function() {
        cc.eventManager.removeListeners(this);
    }

});

var CountDownScene = BasicScene.extend({
    onEnter: function() {
        this._super();
        this.addChild(new CountDownLayer(gameConfig.gameReadyCountDown, function() {
            cc.eventManager.dispatchCustomEvent(GameEvents.gameFinished, {});
        }));
    }
});

var gameSceneManager = function() {
    var self = this;

    function initScenes() {
       return [LandingScene, CountDownScene, GameSeqMatchScene, GameSummaryScene];
    }

    var scenes = [];
    cc.eventManager.addCustomListener(GameEvents.gameFinished, function(event) {
        if (_.isEmpty(scenes)) {
            scenes = initScenes();
        }
        var nextScene = scenes.shift();
        var transition = null;
        var transConfig = nextScene.enterTransition;
        if (transConfig && transConfig.transition && transConfig.duration) {
            transition = new transConfig.transiton(transConfig.duration, new nextScene());
        } else if (transConfig && transConfig.duration) {
            transition = new cc.TransitionFade(transConfig.duration, new nextScene());
        } else {
            transition = new cc.TransitionFade(0.5, new nextScene());
        }
        cc.director.runScene(transition);
    }); //useless Magic
};

var LandingScene = BasicScene.extend({
    ctor: function() {
        this._super();
        var loginSuccessEventListener = {
            event: cc.EventListener.CUSTOM,
            eventName: GameEvents.loginSucceeded,
            callback: function(event) {
                //TODO: handle login logic?
                cc.eventManager.dispatchCustomEvent(GameEvents.gameFinished, {});
                // TODO: acquire challenge id from URL

            }
        };
        this.gameListeners.push(loginSuccessEventListener);
    },
    onEnter: function() {
        this._super();
        var landingLayer = new LandingLayer('zh_cn');
        this.addChild(landingLayer);
    }
});

var GameSeqMatchScene = BasicScene.extend({
    ctor: function() {
        this._super();

    },
    onEnter: function() {
        this._super();
        var self = this;
        var glayer = new GameMatchSeqLayer(10);
        var timeUpListener = {
            event: cc.EventListener.CUSTOM,
            eventName: GameEvents.timeUp,
            callback: function(event) {
                cc.eventManager.dispatchCustomEvent(GameEvents.gameFinished, {game: glayer.name});
            }
        };
        cc.eventManager.addListener(timeUpListener, self);
        var scoreLayer = new ScoreBoardLayer();
        this.scheduleOnce(function() {
            self.addChild(glayer);
        }, cc.delayTime(0.5));
        this.addChild(scoreLayer);
    }
});

var GameSummaryScene = BasicScene.extend({
    ctor: function() {
        this._super();
    },
    onEnter: function() {
        this._super();
        var self = this;
        var glayer = new SummaryLayer(
            GlobalContext.gameSummary
        );
        this.addChild(glayer);
    }
});