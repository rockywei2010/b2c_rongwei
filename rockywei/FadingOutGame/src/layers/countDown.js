var CountDownLayer = cc.Layer.extend({
	ctor:function(countDownSeconds,callback){
		this._super();
		var timer=new GameTimer(countDownSeconds,72,150,callback);
		centerSprite(timer);
		this.addChild(timer);
		timer.start();
	}	
});