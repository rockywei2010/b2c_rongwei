/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 14/10/14
 */
var SummaryLayer = cc.Layer.extend({
    ctor: function(gameSummary) {
        this._super();
        var self = this;
        var fontColor = new cc.Color(35, 52, 66, 255);
        var player1Name = gameSummary.player1.name;
        var player2Name = gameSummary.player2.name;
        var player1NameLabel = new cc.LabelTTF(player1Name, "Arial", 42);
        var player2NameLabel = new cc.LabelTTF(player2Name, "Arial", 42);
        player2NameLabel.x = cc.winSize.width - 200;
        player2NameLabel.color = fontColor;
        player2NameLabel.y = cc.winSize.height - 200;
        player1NameLabel.x = 200;
        player1NameLabel.y = cc.winSize.height - 200;
        player1NameLabel.color = fontColor;

        var vsLabel = new cc.LabelTTF("Vs", "Arial", 42);
        vsLabel.color = fontColor;
        vsLabel.x = cc.winSize.width / 2;
        vsLabel.y = player1NameLabel.y;

        [vsLabel, player1NameLabel, player2NameLabel].forEach(function(label) {
            self.addChild(label);
        });
        var count = 0;

        gameSummary.player1.scores.forEach(function(p1score) {
            var vertical_cor = player1NameLabel.y - 100 * (count + 1);
            var gameNameLabel = new cc.LabelTTF(l18n.getString(p1score.game), "Arial", 32);

            var p2scoreItem = _.find(gameSummary.player2.scores, function(p2score) {
                p2score.game = p1score.game;
            });
            //TODO:remove
            var p2score;
            if(!p2scoreItem) {
                p2score = {score:100};
            } else {
                p2score = p2scoreItem.score;
            }

            var p1scoreLabel = new cc.LabelTTF(p1score.score.toString(), "Arial", 32);
            var p2scoreLabel = new cc.LabelTTF(p2score.score.toString(), "Arial", 32);

            gameNameLabel.x = cc.winSize.width / 2;
            p1scoreLabel.x = player1NameLabel.x;
            p2scoreLabel.x = player2NameLabel.x;

            var colorbar = new ColorBar(50, 50, 300, 50);
            colorbar.y = vertical_cor;
            colorbar.x = cc.winSize.width / 2;

            var portion1 = p1score.score/(p1score.score + p2score.score);
            var portion2= p2score.score/(p1score.score + p2score.score);

            var iteration = 50;
            self.schedule(function() {
                colorbar.update(colorbar.s1 + ((100*portion1)-50) / iteration , colorbar.s2 + ((100*portion2)-50) / iteration);
            }, 0.03, iteration, 1);

            self.addChild(colorbar);
            [gameNameLabel, p1scoreLabel, p2scoreLabel].forEach(function(label) {
                label.color = fontColor;
                self.addChild(label);
                label.y = vertical_cor;
            });
            count++;
        });
    }
});

