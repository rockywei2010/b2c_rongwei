/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 27/10/14
 */

var GameLayer = cc.Layer.extend({
    gameDuration: 20,  /*20*/
    gameDifficulty: 5, /*5,10,15,20*/
    /* 0 -> calculation, 1 -> observation, 2 -> judgement */
    /* 3 -> speed, 4 -> accuracy, 5 -> memory */
    gameType: '',
    ctor: function() {
        this._super();
    }
});
