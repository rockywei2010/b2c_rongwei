/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 14/10/14
 */
var ColorBar = cc.Sprite.extend({
    sbar1: null,
    sbar2: null,
    w: null,
    h: null,
    s1: null,
    s2: null,

    update: function(s1, s2) {
        var self = this;
        self.s1 = s1;
        self.s2 = s2;
        self.sbar1.setScaleX((s1 / (s1 + s2)) * 2 * self.w / self.sbar1._contentSize.width);
        self.sbar1.setScaleY(self.h / self.sbar1._contentSize.height);
        self.sbar2.setScaleX((s2 / (s1 + s2)) * 2 * self.w / self.sbar2._contentSize.width);
        self.sbar2.setScaleY(self.h / self.sbar2._contentSize.height);
        var sbar1Shift = (s2 / (s1 + s2)) * self.w;
        var sbar2Shift = (s1 / (s1 + s2)) * self.w;
        self.sbar1.x = -sbar1Shift;
        self.sbar2.x = sbar2Shift;
    },

    ctor: function(s1, s2, w, h) {
        var self = this;
        this._super();
        self.w = w;
        self.h = h;
        self.sbar1 = new cc.Sprite(res.BalanceBarAzure_png);
        self.sbar2 = new cc.Sprite(res.BalanceBarPink_png);
        self.s1 = s1;
        self.s2 = s2;
        var sbar1Shift = (s2 / (s1 + s2)) * self.w;
        var sbar2Shift = (s1 / (s1 + s2)) * self.w;
        self.sbar1.x = self.x - sbar1Shift;
        self.sbar2.x = self.x + sbar2Shift;
        this.addChild(self.sbar1);
        this.addChild(self.sbar2);
        this.update(s1,s2);
    }
});