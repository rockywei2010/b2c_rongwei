var game = null;

var BasicScene = cc.Scene.extend({
	bgLayer: null,
	gameListeners: null,
	ctor: function() {
		this._super();

		this.gameListeners = [];
	},
	onEnter: function() {
		var self = this;
		self._super();
		self.bgLayer = new BackGroundLayer();

		if (this.gameListeners.length) {
			this.gameListeners.forEach(function(l) {
				cc.eventManager.addListener(l, self);
			});
		} else {

		}

		this.addChild(self.bgLayer);
	},
	onExit: function() {

	}
});

var gameSceneManager = function() {
	var self = this;

	function initScenes() {
		//return [LandingScene,CountDownScene,GameScene];
		return [LandingScene, GameScene];
	}

	//var scenes = [LandingScene,CountDownScene,GameScene];
	var scenes = [LandingScene, GameScene];
	cc.eventManager.addCustomListener(GameEvents.gameFinished, function(event) {
		if (!scenes.length) {
			scenes = initScenes();
		}

		var nextScene = scenes.shift();
		var transition = new cc.TransitionFade(0.5, new nextScene());
		cc.director.runScene(transition);
	});

	//var transConfig = nextScene.enterTransition;
};

var LandingScene = BasicScene.extend({
	ctor: function() {
		this._super();
		var loginSuccessEventListener = {
				event: cc.EventListener.CUSTOM,
				eventName: GameEvents.loginSucceeded,
				callback: function(event) {
					game = event.getUserData().game;
					cc.eventManager.dispatchCustomEvent(GameEvents.gameFinished, event);
				}
		};
		this.gameListeners.push(loginSuccessEventListener);
	},
	onEnter: function() {
		this._super();
		var landingLayer = new LandingLayer();
		this.addChild(landingLayer);
	}
});

var CountDownScene = BasicScene.extend({
	onEnter: function() {
		this._super();
		this.addChild(new CountDownLayer(gameConfig.gameReadyCoutnDown,
				function() {
			cc.eventManager.dispatchCustomEvent(GameEvents.gameFinished, {});
		}));
	}
});


var GameScene = BasicScene.extend({
	cotr: function(event) {
		this._super();
	},
	onEnter: function(event) {
		this._super();
		switch (game) {
		case Games.colorOfDeception:
			var glayer = new GameColorOfDeceptionLayer();
			break;
		case Games.birdWatching:
			var glayer = new GameBirdWatchingLayer();
			break;
		case Games.flickMaster:
			var glayer = new GameFlickMasterLayer();
			break;
		case Games.followTheLeader:
			var glayer = new GameFollowTheLeaderLayer(Sequence.forwards);
			break;
		case Games.unfollowTheLeader:
			var glayer = new GameFollowTheLeaderLayer(Sequence.backwards);
			break;
		case Games.touchTheNumber:
			var glayer = new GameTouchTheNumberLayer();
			break;
		case Games.concentration:
			var glayer = new GameConcentrationLayer();
			break;
		default:
			break;
		}
		var scoreLayer = new ScoreBoardLayer();
		this.scheduleOnce(function() {
			this.addChild(glayer)
		}, cc.delayTime(0.5));
		this.addChild(scoreLayer);
	}
})

var GameSeqMatchScene = BasicScene.extend({
	ctor: function() {
		this._super();
	},

	onEnter: function() {
		this._super();


		//var scoreLayer = new ScoreBoardLayer();
		//this.addChild(glayer)
		this.scheduleOnce(function() {
			this.addChild(glayer)
		}, cc.delayTime(0.5));
		//this.addChild(scoreLayer); 
	}

});