/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 14/10/14
 */
/**
 *  CountDown timer that supports callback
 *  the callback is called only after the timer finished
 *  reset is also available, if a reset is performed after
 *  finish, a 'start' must be called
 *
 *  change the font styles by accessing countdownText, title
 *  @property {cc.LabelTTF} titleText
 *  @property {cc.LabelTTF} countdownText
 */
var GameTimer = cc.Sprite.extend({
    /**
     * Constructor
     * @param {Int} countdown  starting countdown time
     * @param {Int} titleSize  font size of titile text
     * @param {Int} countSize  font size of countdown text
     * @param {Func} callback  callback function after time finishes
     */
    ctor: function(countdown, titleSize, countSize, callback) {
        this._super();
        var self = this;
        self.countdown = countdown;
        self.timeLeft = countdown;
        self.titleText = new cc.LabelTTF(l18n.getString('countDownTitle'), "Arial", titleSize);
        self.countdownText = new cc.LabelTTF(self.timeLeft.toString(), "Arial", countSize);
        self.countdownText.setColor(new cc.Color(35, 52, 66, 255));
        self.titleText.setColor(new cc.Color(35, 52, 66, 255));
        self.titleText.y = self.countdownText.y + (self.titleText.getBoundingBox().height / 2 + self.countdownText.getBoundingBox().height / 2);

        self.addChild(self.countdownText);
        self.addChild(self.titleText);
        self.timerCallback = callback;
    },
    start: function() {
        var self = this;

        self.schedule(function() {
            self.timeLeft--;
            self.countdownText.setString(self.timeLeft.toString());
            if (self.timeLeft === 3 && !self.blinking) {
                self.blinking = true;
                self.countdownText.runAction(cc.blink(3,7));
            }
            if (self.timerCallback && self.timeLeft <= 0) {
                self.timerCallback();
            }
        }, 1, self.countdown, 0);
    },

    reset: function() {
        var self = this;
        self.blinking = false;
        self.titleText.y = self.countdownText.y + (self.titleText.getBoundingBox().height / 2 + self.countdownText.getBoundingBox().height / 2);
        self.timeLeft = self.countdown;
    }
});
