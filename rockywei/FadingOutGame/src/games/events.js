var GameEvents = {
		loginSucceeded: 'loginSucceeded',
		gameFinished: 'gameFinished',
		cardTappedConcentration: 'cardTappedConcentration',
		cardTappedFollowTheLeader: 'cardTappedFollowTheLeader',
		cardTappedTouchTheNumber: 'cardTappedTouchTheNumber',
		cardTappedColorOfDeception: 'cardTappedColorOfDeception',
		cardTappedBirdWatching: 'cardTappedBirdWatching',
		scoreUpdate: 'scoreUpdate',
		mouseLeaveFlickMaster: 'mouseLeaveFlickMaster'
}