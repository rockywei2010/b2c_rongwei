var GameFlickMasterLayer = GameLayer.extend({
	ctor: function(difficulty) {
		this._super();
		var self = this;
		this.reset = function() {
			// sequence: 0逆序， 1 顺序
			self.sequence = Math.floor((Math.random() * 2) + 0);
			self.position = Math.floor((Math.random() * 4) + 0);
			var label = new cc.LabelTTF("", "Arial", 42);
			label.color = new cc.Color(34, 42, 55, 255);
			var card = new Card(cc.winSize.width / 2, cc.winSize.height / 2, label, Games.flickMaster);
			card.rotation = 90 * self.position;
			card.setOpacity(10);
			if (self.sequence == 0) {
				card.setTexture(res.ArrowRed_png)
			} else if (self.sequence == 1) {
				card.setTexture(res.ArrowBlue_png)
			}
			card.runAction(cc.sequence(cc.delayTime(0.3), cc.FadeIn.create(0.3)));
			this.addChild(card);
		}
		this.reset();
		cc.eventManager.addListener(gameFlickMasterListener(self, []), self);
	}
});

var gameFlickMasterListener = function(scene, slot) {
	return {
		event: cc.EventListener.CUSTOM,
		eventName: GameEvents.mouseLeaveFlickMaster,
		callback: function(event) {
			var card = event.getUserData();

			var moveX = card.finalPos.x - card.originalPos.x;
			var moveY = card.finalPos.y - card.originalPos.y;
			var correctMove = false;
			if (scene.sequence == 1) {
				switch (scene.position) {
				case 0:
					if (moveX > 0) {
						correctMove = true;
					}
					break;
				case 1:
					if (moveY < 0) {
						correctMove = true;
					}
					break;
				case 2:
					if (moveX < 0) {
						correctMove = true;
					}
					break;
				case 3:
					if (moveY > 0) {
						correctMove = true;
					}
					break;
				default:
					break;
				}
			} else if (scene.sequence == 0) {
				switch (scene.position) {
				case 0:
					if (moveX < 0) {
						correctMove = true;
					}
					break;
				case 1:
					if (moveY > 0) {
						correctMove = true;
					}
					break;
				case 2:
					if (moveX > 0) {
						correctMove = true;
					}
					break;
				case 3:
					if (moveY < 0) {
						correctMove = true;
					}
					break;
				default:
					break;
				}
			}

			if (correctMove == true) {
				scene.removeChild(card);
				cc.eventManager.dispatchCustomEvent(GameEvents.scoreUpdate, {
					player: 'player1',
					score: 10
				})
				setTimeout(function() {
					scene.reset();
				}, 100);
			} else {
				card.runAction(cc.sequence(new cc.Blink(0.5, 3)));
			}

		}
	}
}