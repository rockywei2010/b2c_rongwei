var Card = cc.Sprite.extend({
	flipDuration: 0.6,
	text: null,
	isAnimating: false,
	flipped: false,
	isReady: false,
	colorMatch: true,
	ctor: function(x, y, text, game) {
		this._super(res.CardBack_png);
		this.text = text;
		this.addChild(text, 5);


		if (text.getString() == Colors.white || text.getString() == Colors.yellow) {
			text.color = new cc.Color(0, 0, 0, 255);
		} else {
			text.color = new cc.Color(255, 255, 255, 255);
		}

		this.text.x += this.width / 2;
		this.text.y += this.height / 2;
		this.x = x;
		this.y = y;
		var self = this;

		cc.eventManager.addListener({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			shallowTouch: true,
			onTouchBegan: function(touch, event) {

				if (!self.isReady) {
					return;
				}
				var location = touch.getLocation();
				if (cc.rectContainsPoint(self.getBoundingBox(), location)) {
					switch (game) {
					case Games.concentration:
						cc.eventManager.dispatchCustomEvent(GameEvents.cardTappedConcentration, self);
						break;
					case Games.followTheLeader:
						cc.eventManager.dispatchCustomEvent(GameEvents.cardTappedFollowTheLeader, self);
						break;
					case Games.touchTheNumber:
						cc.eventManager.dispatchCustomEvent(GameEvents.cardTappedTouchTheNumber, self);
						break;
					case Games.colorOfDeception:
						cc.eventManager.dispatchCustomEvent(GameEvents.cardTappedColorOfDeception, self);
						break;
					case Games.birdWatching:
						cc.eventManager.dispatchCustomEvent(GameEvents.cardTappedBirdWatching, self);
						break;
					default:
						break;
					}

				}
			}
		}, this);



		cc.eventManager.addListener({
			event: cc.EventListener.MOUSE,
			onMouseDown: function(event) {
				self.originalPos = event.getLocation();
			},
			onMouseMove: function(event) {},
			onMouseUp: function(event) {
				switch (game) {
				case Games.flickMaster:
					self.finalPos = event.getLocation();
					cc.eventManager.dispatchCustomEvent(GameEvents.mouseLeaveFlickMaster, self);
					break;
				default:
					break;
				}

			}
		}, this);
	},

	getString: function() {
		return this.text.getString();
	},

	match: function(card) {
		if (this === card) return false;
		return this.getString() === card.getString();
	},

	switchAnimatingStatus: function() {
		this.isAnimating = !this.isAnimating;
	},

	flip: function(callback) {
		cc.log("in flip");
		if (this.isAnimating) {
			return;
		}
		cc.log("in flip2");
		this.switchAnimatingStatus();
		this.flipped = !this.flipped;
		var flipAnimation;
		if (!this.flipped) {
			this.getChildren().forEach(function(c) {
				c.setFlippedX(true);
			});
			// 球面旋转
			// 参数1：执行时间 参数2：起始半径 参数3：半径差
			// 参数4：起始Z角 参数5：旋转Z角差 参数6：起始X角
			// 参数7：旋转X角差
			flipAnimation = new cc.OrbitCamera(this.flipDuration, 1, 0, 360, 180, 0, 0);
		} else {
			this.getChildren().forEach(function(c) {
				c.setFlippedX(false);
			});
			flipAnimation = new cc.OrbitCamera(this.flipDuration, 1, 0, 0, 180, 0, 0);
		}

		var delay = cc.delayTime(this.flipDuration / 2);
		this.runAction(cc.sequence(
				flipAnimation,
				cc.callFunc(this.switchAnimatingStatus(), this)));
		if (!this.text.isFlippedX()) {
			this.text.runAction(cc.sequence(delay, cc.hide()));
		} else {
			this.text.runAction(cc.sequence(delay, cc.show()));
		}
	}
});