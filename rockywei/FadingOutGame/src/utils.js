var centerSprite = function(ccNode) {
	ccNode.x = cc.winSize.width / 2;
	ccNode.y = cc.winSize.height / 2;
}
var charactors = [
                  ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"],
                  ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
                  ["+", "-", "×", "÷", "/", "%", "#", "$", "@"],
                  ["♡", "♥", "★", "☆", "♤", "♠", "♧", "♣", "♢", "♦"]
                  ];

var randomCoordinate = function() {
	var x = Math.floor(Math.random() * charactors.length);
	var y = Math.floor(Math.random() * charactors[x].length);
	return {
		x: x,
		y: y
	};
}
var gameUtils = {
		randomCharactorPairs: function(pair) {
			var selected = [];
			for (var i = 0; i < pair; i++) {
				var cursor = null;
				do {
					cursor = randomCoordinate();
				} while (selected.indexOf(cursor) != -1);
				selected.push(cursor);
			}
			var charPairs = [];
			selected.forEach(function(cursor) {
				charPairs.push(charactors[cursor.x][cursor.y]);
				charPairs.push(charactors[cursor.x][cursor.y]);
			});
			return charPairs;
		}
}
var shuffle = function(o) {
	for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	return o;
}