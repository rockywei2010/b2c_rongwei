/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 14/10/14
 */

l18nlangs.zh_cn = {
    countDownTitle: '准备',
    score: '得分:',
    timer: 'Timer',
    gameMatchSeq: '对对碰'
};
