var res = {
    HelloWorld_png: "res/HelloWorld.png",
    CloseNormal_png: "res/CloseNormal.png",
    CloseSelected_ng: "res/CloseSelected.png",
    /* 120*120 */
    CardBack_png: "res/cardback_almond.png",
    CardBackAlmond_png: "res/cardback_almond.png",
    CardBackArsenic_png: "res/cardback_arsenic.png",
    CardBackAzure_png: "res/cardback_azure.png",
    CardBackBazaar_png: "res/cardback_bazaar.png",
    CardBackBone_png: "res/cardback_bone.png",
    CardBackSkin_png: "res/cardback_skin.png",
    CardBackPink_png: "res/cardback_pink.png",
    CardBackRoundAzure_png: "res/round_azure.png",
    CardBackRoundPink_png: "res/round_pink.png",

    AvatarMask_png: "res/game_avatar_mask.gif",
    BalanceBarAzure_png: "res/balance_bar_azure.png",
    BalanceBarPink_png: "res/balance_bar_pink.png",

    LoginButton: "res/login_button.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
