/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 14/10/14
 */

var GameMatchSeqLayer = GameLayer.extend({
    name: "gameMatchSeq",
    ctor: function (diffculty) {
        if(diffculty) this.gameDifficulty = diffculty;
        var self = this;
        this._super();
        /*
         Mapping Global Game Difficulty to local variables
         e.g. Global Game Difficulty 5 means 3 pairs of card in the game here
         */
        var gameDifficultyMap = {
            cardPair: {
                5: 3,
                10: 4,
                15: 6,
                20: 8
            },
            autoFlip: false
        };

        /*
         The arrangement matrix of cards on the screen
         e.g. if we have 3 pair of card we arrange the card to a 3x2 matrix
         */
        var cardMatrixes = {
            3: [3, 2],
            4: [4, 2],
            6: [4, 3],
            8: [4, 4]
        };

        /* Card visual layout arrangement */
        var cardDimension = {w: 120, h: 120};
        var cardSpacing = 25;
        var cardPair = gameDifficultyMap.cardPair[this.gameDifficulty];
        var cardMatrix = cardMatrixes[cardPair];

        var mainBlock = {
            w: cardDimension.w * cardMatrix[0] + cardSpacing * (cardMatrix[0] - 1),
            h: cardDimension.h * cardMatrix[1] + cardSpacing * (cardMatrix[1] - 1)
        };
        var size = cc.winSize;
        var firstCoordinate = {
            x: (size.width - mainBlock.w + cardDimension.w) / 2,
            y: (size.height - mainBlock.h + cardDimension.h) / 2
        };

        self.reset = function() {
            self.remainingPair = cardPair;
            var charPairs = gameUtils.randomCharactorPairs(cardPair);
            var cards = [];
            charPairs.forEach(function (c) {
                var label = new cc.LabelTTF(c, "Arial", 42);
                    label.color = new cc.Color(35, 52, 66, 255);
                    var card = new Card(cc.winSize.width / 2, cc.winSize.height / 2, label);
                    cards.push(card);
                });
                cards = _.shuffle(cards);
                _.range(cards.length).forEach(function (num) {
                    var card = cards[num];
                    card.x = firstCoordinate.x + (cardDimension.w + cardSpacing) * (num % cardMatrix[0]);
                    card.y = firstCoordinate.y + (cardDimension.h + cardSpacing) * (Math.floor(num / cardMatrix[0]));
                });

                cards.forEach(function (card) {
                    card.text.setOpacity(10);
                    card.setOpacity(10);
                    card.text.runAction(cc.sequence(cc.FadeIn.create(0.3)));
                    card.runAction(cc.sequence(cc.FadeIn.create(0.3)));
                    self.addChild(card);
                });

                var delay = cc.delayTime(2);
                cards.forEach(function (card) {
                    card.runAction(cc.sequence(delay, cc.callFunc(card.flip, card),
                      cc.callFunc(function() {
                        card.isReady = true;
                      })
                    ));
                });
        };
        self.reset();
        /* Visula arrangement ends*/

        /* game event listeners, should be removed when scence is inactive */
        cc.eventManager.addListener(gameMatchSeqCardTapListener(self, []), self);
    }
});

var gameMatchSeqCardTapListener = function (scence, slot) {
    return {
        event: cc.EventListener.CUSTOM,
        eventName: GameEvents.cardTapped,
        callback: function (event) {
            var card = event.getUserData();
            if(card.isAnimating) {
                return;
            }
            if (_.isEmpty(slot)) {
                slot.push(card);
                card.flip();
                return;
            }
            var savedCard = slot.pop();
            var cardPair = [savedCard, card];
            if (savedCard === card) {
                card.flip();
                return
            }
            savedCard.isReady = false;
            card.isReady = false;
            if (savedCard.match(card)) {
                card.flip(function () {
                    cardPair.forEach(function(card) {
                      card.runAction(cc.sequence(
                        new cc.FadeOut(0.1),
                        cc.callFunc(function() {
                          scence.removeChild(savedCard);
                          scence.removeChild(card);
                        })
                      ));
                    });
                    scence.remainingPair -= 1;
                    if(scence.remainingPair === 0) scence.reset();
                    cc.eventManager.dispatchCustomEvent(GameEvents.scoreUpdate,
                      { player:'player1', name:self.name, score: 10 * scence.gameDifficulty })
                });
            } else {
                card.flip(function () {
                    cardPair.forEach(function(card) {
                      card.runAction(
                        cc.sequence(new cc.Blink(0.5, 3),
                        cc.callFunc(card.flip, card),
                        cc.callFunc(function() {card.isReady = true})
                      ))
                    });
                    cc.eventManager.dispatchCustomEvent(GameEvents.scoreUpdate,
                      { player:'player1', score: -10 * scence.gameDifficulty })
                });
            }
        }
    }
}
