var GameBirdWatchingLayer = GameLayer.extend({
	ctor: function() {
		this._super();
		var self = this;
		var cardColorMach = {
				Red: res.CardBackRed_png,
				Blue: res.CardBackBlue_png,
				Black: res.CardBackBlack_png,
				White: res.CardBackWhite_png,
				Yellow: res.CardBackYellow_png,
				Green: res.CardBackGreen_png,
				Brown: res.CardBackBrown_png
		}
		var paris = {
				92: [[5, 4],[6, 3]],
				93: [[5, 2, 2],[6, 1, 2],[5, 1, 3]],
				162:[[9, 7],[10, 6],[11, 5]],
				163:[[7, 6, 3],[7, 4, 5],[8, 7, 1],[8, 6, 2],[8, 5, 3],[8, 4, 4],[9, 6, 1],[9, 5, 2],[9, 4, 3]]
		}
		var levels = {
				5: [2, 9],
				10: [3, 9],
				15: [2, 16],
				55: [3, 16],
		};
		var cardDimension = {
				w: 120,
				h: 120
		};
		var cardSpacing = 25;
		var countCycle = 0;
		
		this.reset = function() {
			countCycle++
			var level

			for (var i = 0; i < Object.keys(levels).length; i++) {
				if (countCycle <= Object.keys(levels)[i]) {
					level = levels[Object.keys(levels)[i]];
					break;
				}
			}

			var mainBlock = {
					w: cardDimension.w * (Math.sqrt(level[1])) + cardSpacing * (Math.sqrt(level[1]) - 1),
					h: cardDimension.h * (Math.sqrt(level[1])) + cardSpacing * (Math.sqrt(level[1]) - 1)
			}
			var size = cc.winSize;
			var firstCoordinate = {
					x: (size.width - mainBlock.w + cardDimension.w) / 2,
					y: (size.height - mainBlock.h + cardDimension.h) / 2
			}
			var pair = paris[level[1] + "" + level[0]][Math.floor(Math.random() * paris[level[1] + "" + level[0]].length) + 0];
			this.cards = [];
			var randomNum = [];
			
			for (var i = 0; i < pair.length; i++) {
				do {
					var found = false;
					var number = Math.floor(Math.random() * Object.keys(cardColorMach).length) + 0;
					for (var j = 0; j < randomNum.length; j++) {
						if (randomNum[j] == number) {
							found = true;
							break
						}
					}
					randomNum.push(number);
				} while (found);

				for (var j = 0; j < pair[i]; j++) {
					var label = new cc.LabelTTF("", "Arial", 42);
					var card = new Card(cc.winSize.width / 2, cc.winSize.height / 2, label, Games.birdWatching);
					card.isReady = true;
					card.setTexture(cardColorMach[Object.keys(cardColorMach)[number]]);
					if (i == 0) {
						card.colorMatch = true;
					} else {
						card.colorMatch = false;
					}
					this.cards.push(card);
				}
			}

			this.cards = shuffle(this.cards);
			
			for (var i = 0; i < this.cards.length; i++) {
				this.cards[i].x = firstCoordinate.x + (cardDimension.w + cardSpacing) * (i % (Math.sqrt(level[1])));
				this.cards[i].y = firstCoordinate.y + (cardDimension.h + cardSpacing) * Math.floor(i / (Math.sqrt(level[1])));
				this.addChild(this.cards[i]);
			}
		}

		this.reset();
		cc.eventManager.addListener(gameBirdWatchingListener(self, []), self);
	}
});


var gameBirdWatchingListener = function(scene, slot) {
	return {
		event: cc.EventListener.CUSTOM,
		eventName: GameEvents.cardTappedBirdWatching,
		callback: function(event) {
			var card = event.getUserData();
			if (card.colorMatch == true) {
				cc.eventManager.dispatchCustomEvent(GameEvents.scoreUpdate, {
					player: 'player1',
					score: 10
				});
				
				for (var i = 0; i < scene.cards.length; i++) {
					scene.removeChild(scene.cards[i]);
				}
				
				setTimeout(function() {
					scene.reset();
				}, 300);
			} else {
				card.runAction(cc.sequence(new cc.Blink(0.5, 3)));
			}
		}
	}
}