/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 14/10/14
 */

var l18n = {
  getString: function(key) {
    return l18nlangs[gameConfig.locale][key];
  }
};

var l18nlangs = {};

