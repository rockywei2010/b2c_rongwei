/**
 * Copyright (C) 2013-2014, VOLATILE BYTES
 * @author: LI Jinglue
 * @date: 11/20/14
 */
var GameEvents = {
    cardTapped: 'CardTapped',
    loginSucceeded: 'loginSucceeded',
    loginFailed: 'loginFailed',
    scoreUpdate: 'scoreUpdate',
    scoreSummary: 'scoreSummary',
    timeUp: 'timeUp',
    gameFinished: 'gameFinished' //Only allowed in scene level
}
