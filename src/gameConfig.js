/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 14/10/14
 */

var gameConfig = {
    /* game lang locale, work with l18n */
    locale: 'zh_cn',
    /* seconds */
    gameReadyCountDown: 5,
    gameDuration: 5
};
