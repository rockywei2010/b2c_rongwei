# BrainAndConquer: Prototype #
-------
### What is Brain and Conquer? ###

* Quick summary

A brain teasing game made for html5 and wechat with cocos2d-x-js and more.

Prototype is the dev-code for this project. This project will be in production!

* version: 1.0 pre-alpha

### Setup ###

* Install cocos2d-x-js, please refer to their official document.
* Clone the repo
* Run bootstrap.sh if you are starting dev for the first time. Make sure cocos cli is available. 

### Contribution guidelines ###
* Code review

  We are currently developing against master branch in the current stage. Please send pull request to lijinglue/b2c.git/master for review.

* Format standards:
  Must use the same code file header and code style file with code existed in repo. Line width 80 chars.

# Credits #
----------
+ ### VolatileByte Team ###
    + Lead
        + [LI Jinglue](abathurli@gmail.com)

 