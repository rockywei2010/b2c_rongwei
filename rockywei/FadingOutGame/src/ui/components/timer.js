var GameTimer = cc.Sprite.extend({
	ctor:function(countdown, titleSize, CountSize,callback){
		this._super();
		var self=this;
		self.countdown = countdown;
		self.timeLeft = countdown;
		self.titleText = new cc.LabelTTF(l18n.getString('countDownTitle'),"Arial", titleSize);
		self.countdownText = new cc.LabelTTF(self.timeLeft.toString(),"Arial",CountSize);
		self.countdownText.setColor(new cc.Color(35, 52, 66, 255));
		self.titleText.setColor(new cc.Color(32, 52, 66, 255));
		self.titleText.y = self.countdownText.y +(self.titleText.getBoundingBox().height/2+self.countdownText.getBoundingBox().height/2);
		self.addChild(self.countdownText);
		self.addChild(self.titleText);
		self.timerCallback=callback;
	},
	start:function(){
		
		this.schedule(function(){
			this.timeLeft--;
			this.countdownText.setString(this.timeLeft.toString());
			if(this.timeLeft===3&&!this.blinking){
				this.blinking=true;
				this.countdownText.runAction(cc.blink(3, 7));
			}
			if(this.timerCallback&&this.timeLeft<=0){
				this.timerCallback();
			}
			
		}, 1, this.countdown, 0)
	},
	
	reset:function(){
		this.blinking=false;
		this.titleText.y=this.countdownText.y+(this.titleText.getBoundingBox().height/2 + this.countdownText.getBoundingBox().height/2);
		this.timeLeft=this.countdown;
	}
});