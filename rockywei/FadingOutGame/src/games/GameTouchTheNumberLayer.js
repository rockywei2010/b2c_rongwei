var GameTouchTheNumberLayer = GameLayer.extend({
	ctor: function(difficulty) {
		this._super();
		var self = this;

		var cardMatrix = [3, 3];
		var countCycle = 0;
		var levels = {
				5: 3,
				10: 4
		}

		var cardDimension = {
				w: 120,
				h: 120
		};
		var cardSpacing = 25;
		var mainBlock = {
				w: cardDimension.w * cardMatrix[0] + cardSpacing * (cardMatrix[0] - 1),
				h: cardDimension.h * cardMatrix[1] + cardSpacing * (cardMatrix[1] - 1)
		}


		var size = cc.winSize;
		var firstCoordinate = {
				x: (size.width - mainBlock.w + cardDimension.w) / 2,
				y: (size.height - mainBlock.h + cardDimension.h) / 2
		}

		this.reset = function() {
			//sequence: 0逆序， 1 顺序
			self.sequence = Math.floor((Math.random() * 2) + 0);
		countCycle++;
		this.cards = [];
		var cardsNum;
		for (var i = 0; i < Object.keys(levels).length; i++) {
			if (countCycle <= Object.keys(levels)[i]) {
				cardsNum = levels[Object.keys(levels)[i]];
				break;
			}
		}
		var backgroundCardNum = cardMatrix[0] * cardMatrix[1];

		for (var i = 0; i < cardsNum; i++) {
			var label = new cc.LabelTTF(i + 1, "Arial", 42);
			label.color = new cc.Color(255, 255, 255, 255);

			var card = new Card(cc.winSize.width / 2, cc.winSize.height / 2, label, Games.touchTheNumber);
			if (self.sequence == 1) {
				card.setTexture(res.CardBackBlue_png);
			} else if (self.sequence == 0) {
				card.setTexture(res.CardBackRed_png);
			}

			card.isReady = true;
			this.cards.push(card);
		}
		if (this.sequence == 1) {
			this.cards[0].isAnimating = true;
		} else {
			this.cards[this.cards.length - 1].isAnimating = true;
		}
		var randomDimension = [];

		for (var i = 0; i < this.cards.length; i++) {
			do {
				var currentDimension = Math.floor((Math.random() * backgroundCardNum) + 0);
			} while (randomDimension.indexOf(currentDimension) != -1);
			randomDimension.push(currentDimension);
			card = this.cards[i];
			card.x = firstCoordinate.x + (cardDimension.w + cardSpacing) * (currentDimension % cardMatrix[0]);
			card.y = firstCoordinate.y + (cardDimension.h + cardSpacing) * (Math.floor(currentDimension / cardMatrix[0]));
			card.text.setOpacity(10);
			card.setOpacity(10);
			card.text.runAction(cc.sequence(cc.FadeIn.create(0.4)));
			card.runAction(cc.sequence(cc.FadeIn.create(0.4)));
			this.addChild(card);
		}
		}
		this.reset();
		cc.eventManager.addListener(gameTouchTheNumberListener(self, []), self);
	}

});

var gameTouchTheNumberListener = function(scene, slot) {
	return {
		event: cc.EventListener.CUSTOM,
		eventName: GameEvents.cardTappedTouchTheNumber,
		callback: function(event) {
			var card = event.getUserData();

			if (card.isAnimating) {
				for (var i = 0; i < scene.cards.length; i++) {
					if (scene.cards[i].isAnimating) {
						if (scene.sequence == 1 && i + 1 < scene.cards.length) {
							var nextCard = scene.cards[i + 1];
							nextCard.isAnimating = true;
						} else if (scene.sequence == 0 && i - 1 >= 0) {
							var nextCard = scene.cards[i - 1];
							nextCard.isAnimating = true;
						}
						scene.cards.splice(i, 1);
						break;
					}
				}
				scene.removeChild(card);
				cc.eventManager.dispatchCustomEvent(GameEvents.scoreUpdate, {
					player: 'player1',
					score: 10
				})
			} else {
				card.runAction(cc.sequence(new cc.Blink(0.5, 3)));
			}

			if (scene.cards.length == 0) {
				setTimeout(function() {
					scene.reset();
				}, 200);

			}
		}
	}
}