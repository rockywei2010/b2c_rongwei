var gameConfig = {
		locale: 'zh_cn',
		gameReadyCoutnDown: 5,
		gameDuration: 20
};

var Games = {
		//		operations: 'operations', // 加减乘除
		flickMaster: 'flickMaster', // 方向达人
		//		simplicity: 'simplicity', // 简易运算
		//		reverseRPS: 'reverseRPS', // 剪刀石头布
		touchTheNumber: 'touchTheNumber', // 升序降序
		followTheLeader: 'followTheLeader', // 顺次击破
		//		additionAddiction: 'additionAddiction', // 点击加法
		//		matching: 'matching', // 对对碰
		colorOfDeception: 'colorOfDeception', // 颜色陷阱
		concentration: 'concentration', //记忆翻牌
		//		highOrLow: 'concentration', //开大开小
		birdWatching: 'birdWatching', //盲头苍蝇
		//		additionLink: 'additionLink', // 相连加法
		//		reflection: 'reflection', // 反光镜
		unfollowTheLeader: 'unfollowTheLeader', // 反序击破
		//		tapTheColor: 'tapTheColor', // 点击色块
		//		rainFall: 'rainFall', // 下雨了
		//		pathToSafety: 'pathToSafety', // 安全之路
		//		touchTheNumberPlus: 'touchTheNumberPlus', // 升序相加
		//		rapidSorting: 'rapidSorting', // 快速排列
		//		quickEye: 'quickEye', // 快速扫描
		//		weatherCast: 'weatherCast' // 天气预报
}

var Colors = {
		red: 'red',
		blue: 'blue',
		black: 'black',
		white: 'white',
		yellow: 'yellow',
		green: 'green',
		brown: 'brown'
}
var CardColorMach = {
		red: res.CardBackRed_png,
		blue: res.CardBackBlue_png,
		black: res.CardBackBlack_png,
		white: res.CardBackWhite_png,
		yellow: res.CardBackYellow_png,
		green: res.CardBackGreen_png,
		brown: res.CardBackBrown_png
}
var Sequence = {
		forwards: 'forwards',
		backwards: 'backwards'
}