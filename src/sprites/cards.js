/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 22/10/14
 */
var Card = cc.Sprite.extend({
    /* const */
    flipDuration: 0.6,

    /* var */
    text: null,
    isAnimating: false,
    flipped: false,
    isReady: false,

    ctor: function (x, y, text) {
        this._super(res.CardBack_png);
        this.text = text;
        this.addChild(text, 5);
        this.text.x += this.width / 2;
        this.text.y += this.height / 2;
        this.x = x;
        this.y = y;
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            shallowTouch: true,
            onTouchBegan: function (touch, event) {
                if(!self.isReady) {
                  return
                }
                var location = touch.getLocation();
                if (cc.rectContainsPoint(self.getBoundingBox(), location)) {
                    cc.eventManager.dispatchCustomEvent(GameEvents.cardTapped, self);
                }
            }
        }, self);
    },

    getString: function () {
        return this.text.getString();
    },

    /* a card only match with another when they are different cards and contain
     * identical texts */
    match: function (card) {
        if (this === card) return false;
        return this.getString() === card.getString()
    },
    switchAnimatingStatus: function () {
        this.isAnimating = !this.isAnimating;
    },

    flip: function (callback) {
        var self = this;
        if (self.isAnimating) {
            cc.log('action rejected, animation not finished:', this.text.getString());
            return;
        }
        this.switchAnimatingStatus();
        this.flipped = !this.flipped;

        var flipAnimation;
        if (!this.flipped) {
            this.getChildren().forEach(function (c) {
                c.setFlippedX(true);
            });
            flipAnimation = new cc.OrbitCamera(this.flipDuration, 1, 0, 360, 180, 0, 0);//cc.flipX3D(this.flipDuration);
        } else {
            this.getChildren().forEach(function (c) {
                c.setFlippedX(false);
            });
            flipAnimation = new cc.OrbitCamera(this.flipDuration, 1, 0, 0, 180, 0, 0);
        }
        //NOTICE: callback is put in to runAction Queue to ensure callback is only excuted after
        //all animation is over
        this.runAction(cc.sequence(
            //cc.callFunc(this.switchAnimatingStatus, this),
            flipAnimation,
            cc.callFunc(this.switchAnimatingStatus, this),
            cc.callFunc(function() { if(_.isFunction(callback)) callback(); })
        ));

        var delay = cc.delayTime(this.flipDuration / 2);
        if (!this.text.isFlippedX()) {
            this.text.runAction(cc.sequence(delay,
                cc.hide()
            ));
        } else {
            this.text.runAction(cc.sequence(delay,
                cc.show()
            ));
        }
    }
});
