/**
 * Copyright (C) 2014 VolatileByte, All Rights Reserved
 * @author: LI Jinglue
 * @date: 23/11/14
 */

/**
 * Landing layer is a welcome page and notifies user to login
 * if possible
 */
var LandingLayer = cc.Layer.extend({
    ctor: function() {
        this._super();
        var menuLoginButton = new cc.MenuItemImage(res.LoginButton,
            res.LoginButton,
            res.LoginButton,
            function() {
                cc.eventManager.dispatchCustomEvent(GameEvents.loginSucceeded, this)
            },
            this
        );
        var landingMenu = new cc.Menu(menuLoginButton);
        this.addChild(landingMenu);
    }
});
