var LandingLayer = cc.Layer.extend({
	ctor: function() {
		this._super();
		
		var menu = new cc.Menu();
		for (var i = 0; i < Object.keys(Games).length; i++) {
			(function(th){
				var key = Object.keys(Games)[i];
				var item = new cc.MenuItemFont(key, function(event) {
					this.startGame(item);	
				}, th);
				item.color = (cc.color(0, 0, 0, 255));
				item.setTag(key)
				menu.addChild(item);
			})(this)
	
		}

		menu.alignItemsVertically();
		this.addChild(menu);

		// var menuLoginButton = new cc.MenuItemImage(
		// res.LoginButton,
		// res.LoginButton,
		// res.LoginButton,
		// function () {
		// cc.eventManager.dispatchCustomEvent(GameEvents.loginSucceeded,this)
		// },
		// this);
		// menuLoginButton.attr({
		// x: 20,
		// y: 20,
		// anchorX: 0.5,
		// anchorY: 0.5
		// });

		// var menu = new cc.Menu(menuLoginButton);
		// this.addChild(menu);
	},
	startGame: function(sender) { 	
		cc.log(sender.getTag())
		this.game=sender.getTag();
		cc.eventManager.dispatchCustomEvent(GameEvents.loginSucceeded,this)
	}
});