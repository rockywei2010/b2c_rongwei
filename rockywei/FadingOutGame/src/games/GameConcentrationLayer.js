var GameConcentrationLayer = GameLayer.extend({
	ctor: function(difficulty) {
		this._super();
		if (difficulty) this.gameDifficulty = difficulty;
		var self = this;
		var gameDificultyMap = {
				cardPair: {
					5: 3,
					10: 4,
					15: 6,
					20: 8
				},
				autoFlip: false
		};

		var cardMatrixes = {
				3: [3, 2],
				4: [4, 2],
				6: [4, 3],
				8: [4, 4]
		}

		var cardDimension = {
				w: 120,
				h: 120
		};
		var cardSpacing = 25;
		var cardPair = gameDificultyMap.cardPair[this.gameDifficulty];
		var cardMatrix = cardMatrixes[cardPair];

		var mainBlock = {
				w: cardDimension.w * cardMatrix[0] + cardSpacing * (cardMatrix[0] - 1),
				h: cardDimension.h * cardMatrix[1] + cardSpacing * (cardMatrix[1] - 1)
		}
		var size = cc.winSize;
		var firstCoordinate = {
				x: (size.width - mainBlock.w + cardDimension.w) / 2,
				y: (size.height - mainBlock.h + cardDimension.h) / 2
		}

		this.reset = function() {

			this.remainPair = cardPair;
			var charPairs = gameUtils.randomCharactorPairs(cardPair);
			var cards = [];
			charPairs.forEach(function(c) {
				var label = new cc.LabelTTF(c, "Arial", 42);
				label.color = new cc.Color(35, 52, 66, 255);

				var card = new Card(cc.winSize.width / 2, cc.winSize.height / 2, label, Games.concentration);
				cards.push(card);
			});

			cards = shuffle(cards);


			for (var i = 0; i < cards.length; i++) {
				card = cards[i];
				card.x = firstCoordinate.x + (cardDimension.w + cardSpacing) * (i % cardMatrix[0]);
				card.y = firstCoordinate.y + (cardDimension.h + cardSpacing) * (Math.floor(i / cardMatrix[0]));

				card.text.setOpacity(10);
				card.setOpacity(10);
				card.text.runAction(cc.sequence(cc.FadeIn.create(1)));
				card.runAction(cc.sequence(cc.FadeIn.create(1)));
				this.addChild(card);
			}

			var delay = cc.delayTime(2);

			cards.forEach(function(card) {
				card.runAction(cc.sequence(delay, cc.callFunc(card.flip, card), cc.callFunc(function() {
					card.isReady = true;
				})))
			});
		};
		this.reset();
		cc.eventManager.addListener(gameConcentrationListener(self, []), self);
	}
});


var gameConcentrationListener = function(scene, slot) {
	return {
		event: cc.EventListener.CUSTOM,
		eventName: GameEvents.cardTappedConcentration,
		callback: function(event) {
			cc.log("will begin flip");
			var card = event.getUserData();
			if (card.isAnimating) {
				return;
			}

			if (slot.length <= 0) {
				slot.push(card);
				card.flip();
				return;
			}
			var savedCard = slot.pop();
			var cardPair = [savedCard, card];
			if (savedCard === card) {
				card.flip();
				return;
			}

			savedCard.isReady = false;
			card.isReady = false;
			if (savedCard.match(card)) {
				cc.log("card will judge yes");
				card.flip(function(card) {
					cardPair.forEach(function(card) {
						card.runAction(cc.sequence(new cc.FadeOut(0.1),
								cc.callFunc(function() {
									cc.log("card will remove yes");
									scene.removeChild(savedCard);
									scene.removeChild(card);
								})
						));
					});
					scene.remainPair -= 1;
					if (scene.remainingPair === 0) {
						scene.reset();
					}

				});
			} else {
				cc.log("card will judge else==>" + cardPair.length);
				card.flip(function() {
					cc.log("in call back");
				});
			}
		}

	}
}